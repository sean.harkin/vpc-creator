#!/bin/bash
sudo apt -y update
sudo apt -y install docker.io
dockerd
apt install awscli
export PUBLIC_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
export KEY_PASSPHRASE=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')
docker volume create --name $OVPN_DATA
docker run -v $OVPN_DATA:/etc/openvpn --log-driver=none --rm kylemanna/openvpn ovpn_genconfig -u udp://$PUBLIC_IP
