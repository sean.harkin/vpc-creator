variable "region" {
  type    = "string"
  default = "eu-west-2"
}

variable "key_name" {
  type    = "string"
  default = "vpn-uk"
}

provider "aws" {
  shared_credentials_file = "/Users/sean/.aws/credentials"
  region                  = var.region
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
resource "aws_security_group" "allow_udp" {
  name        = "allow_udp"
  description = "Allow UDP for openVpn"

  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "ssh" {
  name        = "allow_ssh"
  description = "ssh access"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}


resource "aws_iam_instance_profile" "vpn_profile" {
  name = "vpn_profile2"
  role = aws_iam_role.vpn_role.name
}

resource "aws_iam_role_policy" "vpn_policy" {
  name = "vpn_policy"
  role = aws_iam_role.vpn_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role" "vpn_role" {
  name = "vpn_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_instance" "vpn" {
  ami                  = data.aws_ami.ubuntu.id
  instance_type        = "t3.nano"
  key_name             = var.key_name
  iam_instance_profile = aws_iam_instance_profile.vpn_profile.name
  security_groups      = [aws_security_group.allow_udp.name, aws_security_group.ssh.name]
  user_data            = file("open-vpn-setup.sh")
}

